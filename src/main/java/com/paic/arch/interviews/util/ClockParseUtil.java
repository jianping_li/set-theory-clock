package com.paic.arch.interviews.util;

/**
 * Created by lijianping on 2018-4-11.
 */
public class ClockParseUtil {
    public static final String TIME_FORMAT = "HH:mm:ss";
    private static final String CLOCK_OFF = "O";
    private static final String CLOCK_SECOND_ON = "Y";
    private static final String CLOCK_FIRST_LINE_HOURS_ON = "RRRR";
    private static final String CLOCK_SECOND_LINE_HOURS_ON = "RRRR";
    private static final String CLOCK_FIRST_LINE_MINUTES_ON = "YYRYYRYYRYY";
    private static final String CLOCK_SECOND_LINE_MINUTES_ON = "YYYY";

    /**
     * 解析秒，偶数返回Y，奇数返回0
     *
     * @param seconds
     * @return
     */
    public static String parseSecond(int seconds) {
        return seconds % 2 == 0 ? CLOCK_SECOND_ON : CLOCK_OFF;
    }

    /**
     * 根据小时数获取第一行小时的时钟语言，
     * 逻辑为：获取小时数是5的倍数，倍数代表从左到右要几个灯亮
     *
     * @param hours 小时数
     * @return
     */
    public static String parseFirstLineHours(int hours) {
        int times = hours / 5;
        return convertToClockLan(CLOCK_FIRST_LINE_HOURS_ON, times);
    }

    /**
     * 根据小时数获取第一行小时的时钟语言，
     * 逻辑为：获取小时数%5的余数，余数代表从左到右要几个灯亮
     *
     * @param hours 小时数
     * @return
     */
    public static String parseSecondLineHours(int hours) {
        int remainder = hours % 5;
        return convertToClockLan(CLOCK_SECOND_LINE_HOURS_ON, remainder);
    }

    /**
     * 根据分钟数获取第一行分钟的时钟语言，
     * 逻辑为：获取分钟数/5的倍数，倍数代表从左到右要几个灯亮
     *
     * @param minutes 分钟数
     * @return
     */
    public static String parseFirstLineMinutes(int minutes) {
        int times = minutes / 5;
        return convertToClockLan(CLOCK_FIRST_LINE_MINUTES_ON, times);
    }

    /**
     * 根据分钟数获取第二行分钟的时钟语言，
     * 逻辑为：获取分钟数%5的余数，余数代表从左到右要几个灯亮
     *
     * @param minutes 分钟数
     * @return
     */
    public static String parseSecondLineMinutes(int minutes) {
        int remainder = minutes % 5;
        return convertToClockLan(CLOCK_SECOND_LINE_MINUTES_ON, remainder);
    }

    /**
     * 根据时钟语言模板转换为时钟语言
     *
     * @param lanTemplate 时钟语言模板
     * @param num         需要的位数
     * @return
     */
    private static String convertToClockLan(String lanTemplate, int num) {
        StringBuffer sb = new StringBuffer();
        char[] arys = lanTemplate.toCharArray();
        for (int i = 0; i < arys.length; i++) {
            if (i > num - 1) {
                sb.append(CLOCK_OFF);
            } else {
                sb.append(String.valueOf(arys[i]));
            }
        }
        return sb.toString();
    }
}
