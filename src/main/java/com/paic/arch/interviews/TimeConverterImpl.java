package com.paic.arch.interviews;

import com.paic.arch.interviews.bean.Clock;
import com.paic.arch.interviews.util.ClockParseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by lijianping on 2018-4-11.
 */
public class TimeConverterImpl implements TimeConverter {
    private static final Logger LOG = LoggerFactory.getLogger(TimeConverterImpl.class);

    private static final String COLON = ":";

    @Override
    public String convertTime(String aTime) {
        //第一步：判断传入字符串格式
        SimpleDateFormat dateFormat = new SimpleDateFormat(ClockParseUtil.TIME_FORMAT);
        try {
            dateFormat.parse(aTime);
        } catch (ParseException e) {
            LOG.error("时间格式错误");
            throw new RuntimeException("传入参数非正确的HH:mm:ss格式");
        }
        //第二步，构造Clock对象并返回展示
        String[] timeArgs = aTime.split(COLON);
        int hours = Integer.valueOf(timeArgs[0]);
        int minutes = Integer.valueOf(timeArgs[1]);
        int seconds = Integer.valueOf(timeArgs[2]);

        Clock clock = new Clock(ClockParseUtil.parseSecond(seconds),
                ClockParseUtil.parseFirstLineHours(hours),
                ClockParseUtil.parseSecondLineHours(hours),
                ClockParseUtil.parseFirstLineMinutes(minutes),
                ClockParseUtil.parseSecondLineMinutes(minutes));
        return clock.display();
    }
}
