package com.paic.arch.interviews;

public interface TimeConverter {

    /**
     * 根据时间"HH:mm:ss"生成Colck对象，返回对应的时钟语言
     *
     * @param aTime
     * @return
     */
    String convertTime(String aTime);

}
