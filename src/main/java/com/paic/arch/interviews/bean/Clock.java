package com.paic.arch.interviews.bean;

/**
 * Created by lijianping on 2018-4-11.
 */
public class Clock {
    private String second;//第一行秒Y代表亮灯，O代表不亮
    private String firstLineHours;//第一行小时，一盏亮灯R代表5小时，O代表不亮 RROO-10小时
    private String secondLineHours;//第二行小时，一盏亮灯R代表1小时，O代表不亮 RROO-2小时
    private String firstLineMinuts;//第一行分钟，一盏亮灯Y或者R代表5分钟，0代表不亮 YYRYYR00000-30分钟
    private String secondLineMinuts;//第二行分钟，一盏亮灯Y代表1分钟，0代表不亮 YY00-2分钟

    public Clock(String second, String firstLineHours, String secondLineHours, String firstLineMinuts, String secondLineMinuts) {
        this.second = second;
        this.firstLineHours = firstLineHours;
        this.secondLineHours = secondLineHours;
        this.firstLineMinuts = firstLineMinuts;
        this.secondLineMinuts = secondLineMinuts;
    }

    /**
     * 时钟语言显示
     * <p>
     * Scenario: Just before midnight
     * When the time is 23:59:59
     * Then the clock should look like
     * <blockquote><pre>
     * O
     * RRRR
     * RRRO
     * YYRYYRYYRYY
     * YYYY
     * </pre></blockquote>
     * </p>
     */
    public String display() {
        StringBuilder sb = new StringBuilder();
        String lineSeparator = String.valueOf(System.getProperties().get("line.separator"));
        sb.append(second).append(lineSeparator)
                .append(firstLineHours).append(lineSeparator)
                .append(secondLineHours).append(lineSeparator)
                .append(firstLineMinuts).append(lineSeparator)
                .append(secondLineMinuts);
        return sb.toString();
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getFirstLineHours() {
        return firstLineHours;
    }

    public void setFirstLineHours(String firstLineHours) {
        this.firstLineHours = firstLineHours;
    }

    public String getSecondLineHours() {
        return secondLineHours;
    }

    public void setSecondLineHours(String secondLineHours) {
        this.secondLineHours = secondLineHours;
    }

    public String getFirstLineMinuts() {
        return firstLineMinuts;
    }

    public void setFirstLineMinuts(String firstLineMinuts) {
        this.firstLineMinuts = firstLineMinuts;
    }

    public String getSecondLineMinuts() {
        return secondLineMinuts;
    }

    public void setSecondLineMinuts(String secondLineMinuts) {
        this.secondLineMinuts = secondLineMinuts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Clock clock = (Clock) o;

        if (second != null ? !second.equals(clock.second) : clock.second != null) return false;
        if (firstLineHours != null ? !firstLineHours.equals(clock.firstLineHours) : clock.firstLineHours != null)
            return false;
        if (secondLineHours != null ? !secondLineHours.equals(clock.secondLineHours) : clock.secondLineHours != null)
            return false;
        if (firstLineMinuts != null ? !firstLineMinuts.equals(clock.firstLineMinuts) : clock.firstLineMinuts != null)
            return false;
        return secondLineMinuts != null ? secondLineMinuts.equals(clock.secondLineMinuts) : clock.secondLineMinuts == null;

    }

    @Override
    public int hashCode() {
        int result = second != null ? second.hashCode() : 0;
        result = 31 * result + (firstLineHours != null ? firstLineHours.hashCode() : 0);
        result = 31 * result + (secondLineHours != null ? secondLineHours.hashCode() : 0);
        result = 31 * result + (firstLineMinuts != null ? firstLineMinuts.hashCode() : 0);
        result = 31 * result + (secondLineMinuts != null ? secondLineMinuts.hashCode() : 0);
        return result;
    }
}
